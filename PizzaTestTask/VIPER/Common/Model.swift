//
//  Model.swift
//  PizzaTestTask
//
//  Created by Sasha Styazhkin on 13.12.2021.
//

import Foundation

func getIngredientsArray (menuItem: MenuItemModel?) -> [String] {
    var ingredientsArray: [String] = []
    if let sections = menuItem?.sections {
        for component in sections[0].components {
            ingredientsArray.append(component.raw_text)
        }
    }
    
    return ingredientsArray
}

extension Array {
    func chunked(into size: Int) -> [[Element]] {
        return stride(from: 0, to: count, by: size).map {
            Array(self[$0 ..< Swift.min($0 + size, count)])
        }
    }
}
