//
//  NetworkRequest.swift
//  PizzaTestTask
//
//  Created by Sasha Styazhkin on 29.10.2021.
//

import Foundation

class NetworkRequest {
    
    static let shared = NetworkRequest()
    
    private init() {}
    
    func requestData(urlString: String, completion: @escaping (Result<Data, Error>) -> Void) {
        
        guard let url = URL(string: urlString) else { return }
        
        URLSession.shared.dataTask(with: url) { data, responce, error in
            DispatchQueue.main.async {
                if let error = error {
                    completion(.failure(error))
                    return
                }
                
                guard let data = data else { return }
                completion(.success(data))
            }
        }
        
        .resume()
        
    }
    
    func apiRequest(searchedItemName: String?, completion: @escaping (Result<Data, Error>) -> Void) {
        
        // API info
        // change if needed
        
        let headers = [
            "x-rapidapi-host": "tasty.p.rapidapi.com",
            "x-rapidapi-key": "f3e48e8a8emsh81851d8ea658263p10a5c8jsn31fc5b3dfde5"
        ]

        let request = NSMutableURLRequest(url: NSURL(string: "https://tasty.p.rapidapi.com/recipes/list?from=0&size=2&q=\(searchedItemName ?? "pizza")")! as URL,
                                                cachePolicy: .useProtocolCachePolicy,
                                            timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers

        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest) { [weak self] (data, response, error) -> Void in
            
            DispatchQueue.main.async {
                if error != nil {
                    completion(.failure(error as! Error))
                }
                
                guard let data = data else { return }
                completion(.success(data))
            }
            
        }
        
        dataTask.resume()
        
    }
    
}
