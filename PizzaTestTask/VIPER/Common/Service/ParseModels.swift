//
//  ParseModels.swift
//  PizzaTestTask
//
//  Created by Sasha Styazhkin on 29.10.2021.
//

import Foundation


struct ApiResult: Codable {
    let count: Int
    let results: [MenuItemModel]
}

struct MenuItemModel: Codable {
    let id: Int
    let name: String
    let thumbnail_url: String?
    let sections: [SectionItem]?
}

struct SectionItem: Codable {
    let components: [ComponentItem]
}

struct ComponentItem: Codable {
    let raw_text: String
}
