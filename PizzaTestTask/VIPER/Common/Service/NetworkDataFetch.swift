//
//  NetworkDataFetch.swift
//  PizzaTestTask
//
//  Created by Sasha Styazhkin on 29.10.2021.
//

import Foundation

class NetworkDataFetch {
    
    static let shared = NetworkDataFetch()
    
    private init() {}
    
    func fetchResult (searchedItemName: String, responce: @escaping (ApiResult?, Error?) -> Void) {
        
        NetworkRequest.shared.apiRequest(searchedItemName: searchedItemName) { result in
            
            switch result {
            
            case .success(let data):
                do {
                    let result = try JSONDecoder().decode(ApiResult.self, from: data)
                    responce(result, nil)
                } catch let jsonError {
                    print("JSON decode failure in fetch result.", jsonError)
                }
                
            case .failure(let error):
                print("Error with data.", error.localizedDescription)
                responce(nil, error)
                
            }
        }
        
    }
    
}
