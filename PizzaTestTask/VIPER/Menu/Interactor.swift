//
//  Interactor.swift
//  PizzaTestTask
//
//  Created by Sasha Styazhkin on 14.12.2021.
//

import Foundation

protocol AnyInteractor {
    var presenter: AnyPresenter? { get set }
    
    func getItems(categories: [String])
}

class MenuInteractor: AnyInteractor {
    
    
    var presenter: AnyPresenter?
    
    func getItems(categories: [String]) {
        
        var items = [MenuItem]()
        var itemsOfSection = [String: [MenuItem]]()
        let group = DispatchGroup()
        
        for category in categories {
            group.enter()
            NetworkDataFetch.shared.fetchResult(searchedItemName: category) { [weak self] resultModel, error in
                if error != nil {
                    self?.presenter?.interactorDidFetchItems(with: .failure(FetchError.failed))
                } else {
                    guard let resultModel = resultModel else { return }
                    itemsOfSection[category] = []
                    for item in resultModel.results {
                        itemsOfSection[category]?.append(MenuItem(id: item.id, name: item.name, thumbnailUrl: item.thumbnail_url, ingredients: getIngredientsArray(menuItem: item)))
                    }
                    group.leave()
                }
            }

        }
        group.notify(queue: .main) {
            for category in categories {
                items.append(contentsOf: itemsOfSection[category] ?? [])
            }
            self.presenter?.interactorDidFetchItems(with: .success(items))
        }
    }
}
