//
//  Entity.swift
//  PizzaTestTask
//
//  Created by Sasha Styazhkin on 14.12.2021.
//

import Foundation

struct Item {
    let name: String
}

struct MenuItem {
    let id: Int
    let name: String
    let thumbnailUrl: String?
    let ingredients: [String]?
}
