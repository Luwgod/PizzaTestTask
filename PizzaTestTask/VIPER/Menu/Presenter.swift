//
//  Presenter.swift
//  PizzaTestTask
//
//  Created by Sasha Styazhkin on 14.12.2021.
//

import Foundation

enum FetchError: Error  {
    case failed
}

protocol AnyPresenter {
    var router: AnyRouter? { get set }
    var interactor: AnyInteractor? { get set}
    var view: AnyView? { get set }
    
    func interactorDidFetchItems(with result: Result<[MenuItem], Error>)
}

class MenuPresenter: AnyPresenter {
    
    
    var router: AnyRouter?
    
    var interactor: AnyInteractor? 
//        didSet {
//            interactor?.getItems(categories: view?.categories ?? [])
//        }
    
    
    var view: AnyView? {
        didSet {
            interactor?.getItems(categories: view?.categories ?? [])
        }
    }
    
    func interactorDidFetchItems(with result: Result<[MenuItem], Error>) {
        switch result {
        case .success(let items):
            // update view with items
            self.view?.update(with: items )
        case .failure:
            // update view with error
            self.view?.update(with: "Something went wrong")
        }
    }
    
}
