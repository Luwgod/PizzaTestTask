//
//  View.swift
//  PizzaTestTask
//
//  Created by Sasha Styazhkin on 14.12.2021.
//

import Foundation
import UIKit

protocol AnyView {
    var presenter: AnyPresenter? { get set }
    
    var categories: [String] { get set }
    
    func update(with items: [MenuItem])
    func update(with error: String)
}

class MenuViewController: UIViewController, AnyView, UICollectionViewDelegate, UICollectionViewDataSource {
   
    var categories = ["Pizza", "Drink", "Potato", "Soup", "Burger"]
    
    var presenter: AnyPresenter?
    

    @IBAction func categoryButtonAction(_ sender: UIButton) {
        guard let indexOfCategory = categories.lastIndex(of: (sender.titleLabel?.text)!) else { return }
        scrollToItem(indexOfCategory: indexOfCategory)
    }
    
    
    @IBOutlet weak var cityMenuView: UIView!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var bannerCollectionView: UICollectionView!
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    @IBOutlet weak var menuCollectionView: UICollectionView!
    @IBOutlet weak var topShadowView: UIView!
    @IBOutlet weak var menuHeightConstraint: NSLayoutConstraint!
    
    var menuItems = [[MenuItem]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setCollectionViews()
        setShadows()
    }
    
    
    // MARK: - Collection View Delegate functions
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView {
        case menuCollectionView:
            if let section = menuItems.first {
                return section.count
            } else { return 0 }
        case bannerCollectionView:
            return 2
        case categoryCollectionView:
            return categories.count
        default:
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch collectionView {
        
        case menuCollectionView:
            let cell = menuCollectionView.dequeueReusableCell(withReuseIdentifier: "menuItemCell", for: indexPath) as! MenuItemCollectionViewCell
            cell.configureMenuItemCell(item: menuItems[indexPath.section][indexPath.row])
            return cell
            
        case categoryCollectionView:
            let cell = categoryCollectionView.dequeueReusableCell(withReuseIdentifier: "categoryCell", for: indexPath) as! CategoryCollectionViewCell
            cell.configureCategoryCell(category: categories[indexPath.row])
            return cell
            
        case bannerCollectionView:
            let cell = bannerCollectionView.dequeueReusableCell(withReuseIdentifier: "bannerCell", for: indexPath) as! BannerCollectionViewCell
            cell.configureBannerCell()
            return cell
            
        default:
            return UICollectionViewCell()
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == menuCollectionView {
            return menuItems.count
        }
        return 1
    }
    
    
    // MARK: - Other UI functions
    
        
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        self.menuHeightConstraint.constant = menuCollectionView.contentSize.height
    }
    
    
    func scrollToItem(indexOfCategory: Int) {
        guard let cell = menuCollectionView.cellForItem(at: IndexPath(item: 0, section: indexOfCategory)) else { return }
        var newPosition = CGPoint(x: 0, y: cell.center.y)
        if indexOfCategory == 0 {
            let topOffset = CGPoint(x: 0, y: bannerCollectionView.frame.maxY - view.safeAreaInsets.top)
            mainScrollView.setContentOffset(topOffset, animated: true)
            return
        }
        if indexOfCategory == categories.count - 1 {
            let bottomOffset = CGPoint(x: 0, y: mainScrollView.contentSize.height - mainScrollView.bounds.height + mainScrollView.contentInset.bottom + view.safeAreaInsets.bottom)
            mainScrollView.setContentOffset(bottomOffset, animated: true)
            return
        }
        
        mainScrollView.setContentOffset(newPosition, animated: true)
    }
    
    func update(with items: [MenuItem]) {
        self.menuItems = items.chunked(into: 2)
        menuCollectionView.reloadData()
        DispatchQueue.main.async {
            self.viewWillLayoutSubviews()
        }
    }
    
    func update(with error: String) {
        self.menuItems = []
        print(error)
        DispatchQueue.main.async {
            self.viewWillLayoutSubviews()
        }
    }
    
    // MARK: - Functions to set view in viewDidLoad method
    
    func setCollectionViews() {
        bannerCollectionView.delegate = self
        bannerCollectionView.dataSource = self
        categoryCollectionView.delegate = self
        categoryCollectionView.dataSource = self
        menuCollectionView.delegate = self
        menuCollectionView.dataSource = self
        
        menuCollectionView.layer.cornerRadius = 30
    }
    
    func setShadows() {
        topShadowView.layer.masksToBounds = false
        
        topShadowView.layer.shadowColor = UIColor(red: 0.646, green: 0.646, blue: 0.646, alpha: 0.24).cgColor
        topShadowView.layer.shadowRadius = 14
        topShadowView.layer.shadowOffset = CGSize(width: 0, height: 0)
        topShadowView.layer.shadowOpacity = 1
        let shadowSize: CGFloat = 30
        let shadowPath = CGPath(ellipseIn: CGRect(x: -shadowSize, y: topShadowView.bounds.height - shadowSize * 0.5, width: topShadowView.bounds.width + shadowSize * 2, height: shadowSize), transform: nil)
        topShadowView.layer.shadowPath = shadowPath
    }
    
}
