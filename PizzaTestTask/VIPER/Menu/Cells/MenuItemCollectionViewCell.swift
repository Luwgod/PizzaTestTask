//
//  MenuItemCollectionViewCell.swift
//  PizzaTestTask
//
//  Created by Sasha Styazhkin on 28.10.2021.
//

import UIKit

class MenuItemCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var menuItemImageView: UIImageView!
    
    @IBOutlet weak var menuItemNameLabel: UILabel!
    
    @IBOutlet weak var menuItemContentsLabel: UILabel!
    
    
    func configureMenuItemCell(item: MenuItem) {
        menuItemNameLabel.text = item.name
        menuItemContentsLabel.text = item.ingredients?.joined(separator: ",")
        if let urlString = item.thumbnailUrl {
        NetworkRequest.shared.requestData(urlString: urlString) { [weak self] result in
            switch result {
            case .success(let data):
                let image = UIImage(data: data)
                self?.menuItemImageView.image = image
            case .failure(let error):
                self?.menuItemImageView.image = nil
                print("No image.", error.localizedDescription)
            }
        }
        }
        
        menuItemImageView.layer.cornerRadius = menuItemImageView.layer.frame.width / 2
    }
    
    
    
    
}
