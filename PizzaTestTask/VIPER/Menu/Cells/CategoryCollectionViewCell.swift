//
//  CategoryCollectionViewCell.swift
//  PizzaTestTask
//
//  Created by Sasha Styazhkin on 28.10.2021.
//

import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {
    

    @IBAction func categoryButtonAction(_ sender: UIButton) {
    }
    
    @IBOutlet weak var categoryButton: UIButton!
    
    func configureCategoryCell(category name: String) {
        self.categoryButton.setTitle(name, for: .normal)
    }
    
}
