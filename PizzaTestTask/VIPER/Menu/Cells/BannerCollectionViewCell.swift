//
//  BannerCollectionViewCell.swift
//  PizzaTestTask
//
//  Created by Sasha Styazhkin on 28.10.2021.
//

import UIKit

class BannerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var bannerImageView: UIImageView!
    
    func configureBannerCell() {
        self.bannerImageView.image = UIImage(named: "banner")
    }
}
