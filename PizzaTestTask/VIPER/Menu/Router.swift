//
//  Router.swift
//  PizzaTestTask
//
//  Created by Sasha Styazhkin on 14.12.2021.
//

import Foundation
import UIKit

typealias EntryPoint = AnyView & UIViewController

protocol AnyRouter {
    var entry: EntryPoint? { get }
    
    static func start() -> AnyRouter & TabBarViewProtocol
}


class MenuRouter: AnyRouter, TabBarViewProtocol {
    
    var tabIcon: UIImage = UIImage(named: "Menu")!
    var tabTitle: String = "Меню"
    
    
    
    var entry: EntryPoint?
    
    static func start() -> AnyRouter & TabBarViewProtocol {
        let router = MenuRouter()
        
        var view: AnyView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "MenuViewController") as! MenuViewController
        var interactor: AnyInteractor = MenuInteractor()
        var presenter: AnyPresenter = MenuPresenter()
        
        view.presenter = presenter
        
        interactor.presenter = presenter
        
        presenter.router = router
        presenter.interactor = interactor
        presenter.view = view
        
        router.entry = view as? EntryPoint
        
        
        return router
    }
    
    func configuredViewController() -> UIViewController {
        return self.entry!
    }
    
}
