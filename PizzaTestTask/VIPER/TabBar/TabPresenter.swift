//
//  TabPresenter.swift
//  PizzaTestTask
//
//  Created by Sasha Styazhkin on 15.12.2021.
//

import Foundation

protocol TabPresenterProtocol {
    var view: TabViewProtocol? { get set }
    var interactor: TabInteractorProtocol? { get set }
    var router: TabRouterProtocol? { get set }
    
}

class TabPresenter: TabPresenterProtocol {
    var view: TabViewProtocol?
    
    var interactor: TabInteractorProtocol?
    
    var router: TabRouterProtocol?
    
    
}
