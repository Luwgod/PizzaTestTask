//
//  TabInteractor.swift
//  PizzaTestTask
//
//  Created by Sasha Styazhkin on 15.12.2021.
//

import Foundation

protocol TabInteractorProtocol {
    var presenter: TabPresenterProtocol? { get set }
    
}

class TabInteractor: TabInteractorProtocol {
    var presenter: TabPresenterProtocol?
    
    
}
