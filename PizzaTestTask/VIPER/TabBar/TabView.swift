//
//  TabView.swift
//  PizzaTestTask
//
//  Created by Sasha Styazhkin on 15.12.2021.
//

import Foundation
import UIKit

protocol TabViewProtocol {
    var presenter: TabPresenterProtocol? { get set }
}

class TabViewController: UITabBarController, TabViewProtocol, UITabBarControllerDelegate {
    
    var presenter: TabPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}
