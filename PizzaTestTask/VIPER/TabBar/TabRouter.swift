//
//  TabRouter.swift
//  PizzaTestTask
//
//  Created by Sasha Styazhkin on 15.12.2021.
//

import Foundation
import UIKit

typealias TabEntryPoint = TabViewProtocol & UITabBarController

protocol TabRouterProtocol {
    var entry: TabEntryPoint? { get }
    
    static func start(routes: [TabBarViewProtocol]) -> TabRouter
}


class TabRouter: TabRouterProtocol {
    var entry: TabEntryPoint?
    
    static func start(routes: [TabBarViewProtocol]) -> TabRouter {
        let router = TabRouter()
        
        var view: TabViewProtocol = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "TabBarController") as! TabViewController
        var interactor: TabInteractorProtocol = TabInteractor()
        var presenter: TabPresenterProtocol = TabPresenter()
        
        view.presenter = presenter
        
        interactor.presenter = presenter
        
        presenter.router = router
        presenter.interactor = interactor
        presenter.view = view
        
        
        
        var viewControllers = [UIViewController]()
        
        for route in routes {
            let tabBarItem = UITabBarItem()
            tabBarItem.image = route.tabIcon
            tabBarItem.title = route.tabTitle
            let viewController = route.configuredViewController()
            let navigationController = UINavigationController(rootViewController: viewController)
            navigationController.tabBarItem = tabBarItem
            navigationController.setNavigationBarHidden(true, animated: false)
            navigationController.title = route.tabTitle
            viewControllers.append(navigationController)
        }
        
        router.entry = view as? TabEntryPoint
        router.entry?.viewControllers = viewControllers
        
        return router
    }
    
}
