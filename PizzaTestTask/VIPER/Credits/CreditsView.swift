//
//  CreditsView.swift
//  PizzaTestTask
//
//  Created by Sasha Styazhkin on 15.12.2021.
//

import Foundation
import UIKit

protocol CreditsViewProtocol {
    var presenter: CreditsPresenterProtocol? { get set }
}

class CreditsViewController: UIViewController, CreditsViewProtocol {
    var presenter: CreditsPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}
