//
//  CreditsRouter.swift
//  PizzaTestTask
//
//  Created by Sasha Styazhkin on 15.12.2021.
//

import Foundation
import UIKit

typealias CreditsEntryPoint = CreditsViewProtocol & UIViewController

protocol CreditsRouterProtocol {
    var entry: CreditsEntryPoint? { get }
    
    static func start() -> CreditsRouterProtocol & TabBarViewProtocol
}


class CreditsRouter: CreditsRouterProtocol, TabBarViewProtocol {
    
    var tabIcon: UIImage = UIImage(named: "Credits")!
    var tabTitle: String = "Контакты"
    
    
    
    var entry: CreditsEntryPoint?
    
    static func start() -> CreditsRouterProtocol & TabBarViewProtocol {
        let router = CreditsRouter()
        
        var view: CreditsViewProtocol = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "CreditsViewController") as! CreditsViewController
        var interactor: CreditsInteractorProtocol = CreditsInteractor()
        var presenter: CreditsPresenterProtocol = CreditsPresenter()
        
        view.presenter = presenter
        
        interactor.presenter = presenter
        
        presenter.router = router
        presenter.interactor = interactor
        presenter.view = view
        
        router.entry = view as? CreditsEntryPoint
        
        
        return router
    }
    
    func configuredViewController() -> UIViewController {
        return self.entry!
    }
    
}
