//
//  CreditsPresenter.swift
//  PizzaTestTask
//
//  Created by Sasha Styazhkin on 15.12.2021.
//

import Foundation

protocol CreditsPresenterProtocol {
    var view: CreditsViewProtocol? { get set }
    var interactor: CreditsInteractorProtocol? { get set }
    var router: CreditsRouterProtocol? { get set }
    
}

class CreditsPresenter: CreditsPresenterProtocol {
    var view: CreditsViewProtocol?
    
    var interactor: CreditsInteractorProtocol?
    
    var router: CreditsRouterProtocol?
    
    
}
