//
//  CreditsInteractor.swift
//  PizzaTestTask
//
//  Created by Sasha Styazhkin on 15.12.2021.
//

import Foundation

protocol CreditsInteractorProtocol {
    var presenter: CreditsPresenterProtocol? { get set }
    
}

class CreditsInteractor: CreditsInteractorProtocol {
    var presenter: CreditsPresenterProtocol?
    
    
}
